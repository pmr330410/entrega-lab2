---
title: "Artigos publicados"
date: 2021-09-18T23:27:57-03:00
draft: false
---

Aqui encontra-se uma lista com meus artigos publicados.

1. Soccer Player Pose Recognition in Games (https://link.springer.com/chapter/10.1007/978-3-031-13588-0_49)

- Rodrigo G. Reis, Diego P. Trachtinguerts, André K. Sato, Rogério Y. Takimoto, Fábio S. G. Tsuzuki,  Marcos de Sales Guerra Tsuzuki.
- International Conference on Geometry and Graphics, 2022.
- Este artigo foi elaborado durante um programa de Iniciação Científica, focada em aplicações de Visão Computacional no futebol. 
Foi realizada entre set./2021 e ago./2022. 


