Olá! Sou o Rodrigo Gebara Reis, e você está em meu portfólio.

Este site começou como uma entrega de trabalho, mas acabou por ser uma ferramenta útil
para resumir informações sobre mim e divulgar meu trabalho.

Aqui, você encontrará:

- Minha biografia
- Meu currículo
- Meus artigos publicados