---
title: "Sobre mim"
date: 2022-09-22T18:08:08-03:00
draft: false
layout: about
---

Nasci em Campo Grande, MS, em 1 de junho de 2002. 

Desde os 11 meses de idade, passei a frequentar o berçário de uma escola local, o Colégio Harmonia, que passaria a ser minha segunda casa.
Nela, comecei a aprender a falar em Inglês desde antes do Ensino Fundamental, no dito K3, e, como uma boa escola de cidade "pequena", fiz vários
amigos.

Com o passar dos anos, permaneci sempre nesse mesmo colégio, com os mesmos colegas de classe. Isso gerou um crescimento pessoal e emocional
muito grande, mas ao mesmo tempo uma certa dependência. Como estava sempre perto das mesmas pessoas, era estranho me relacionar com outras, de
outro círculo de amizades. Isso perdura até hoje, não tenho facilidade em conhecer novas pessoas, mas depois que me enturmo é outra história! 

De qualquer forma, no Ensino Fundamental fui introduzido à Computação, à Robótica e, claro, à Matemática e à Física. Essas últimas, principalmente,
sempre chamaram minha atenção. Tinha facilidade, e gostava de compreender o motivo de porque determinadas coisas são como são. Com Robótica, fiz parte
de um grupo de extensão pequeno, que possibilitou um aprendizado intenso de C++ e Arduino, em um curto período de tempo. Gostei muito de "brincar" com 
programação, e ver as automatizações que propunha funcionando.

Ainda no Colégio Harmonia, agora no Ensino Médio, não sabia bem o que fazer depois de sair da escola. Havia a pressão do vestibular, da escolha de curso, 
mas não tinha ideia do que gostaria de cursar. O sonho da minha mãe, assim como de muitas por aí, era que eu fosse aprovado em Medicina. Já sabia que não
era algo para mim, mas mesmo assim, no primeiro ano, me inscrevi em um Workshop de Medicina, proposto pela Universidade Federal do MS.

Achei muito interessante, mas, como esperado, não me imaginava estudando essa área, e muito menos trabalhando nela. Com isso, deixei essa decisão para o 
Rodrigo do futuro. Aproveitei o restante do primeiro e segundo anos, e, nesse período, aprendi muito na escola e vida.

Rápido demais, contudo, chegou o temido "Terceirão". Como já disse, Robótica, Programação
e Matemática me despertavam um interesse tremendo, então busquei opções relacionadas a isso. 

