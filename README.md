Este repositório foi criado para a primeira entrega de Laboratório de PMR3304 - Sistemas de Informação (2022). Ela visa a criação de sites estáticos com o uso da ferramenta 'Hugo'. 

O repositório contém duas branches, organizadas da seguinte forma:

1. A branch 'main' possui apenas o tutorial sugerido no e-Disciplinas, com commits que não devem ser considerados na correção.
2. A branch 'entrega' possui o código efetivamente desenvolvido para a entrega, e deve ser utilizada para a correção do exercício. O primeiro commit contém os arquivos advindos do tutorial (estágio inicial de desenvolvimento).